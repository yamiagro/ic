# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 20:45:04 2018

@author: yamia
"""
import datetime
import sys

archivo = open(sys.argv[1])
nombre_salida = sys.argv[2] 
n = int(sys.argv[3])

def separararchivo(archivo):
    datos = []
    for linea in archivo:
        datos.append(linea.split(","))
    return datos

def mediciones_float(mediciones): #paso los caracteres a números
    mediciones_fl_sl=[]
    mediciones_fl=[]
    for fila in mediciones:
        mediciones_fl_sl=[]
        for dato in fila:
            try:
                mediciones_fl_sl.append(float(dato))
            except:
                mediciones_fl_sl.append('NA')
        mediciones_fl.append(mediciones_fl_sl)
    return mediciones_fl
      
def promedio(mediciones,n):
    suma=0
    promedio=0
    tabla_prom=[]
    tabla_promedio=[]
    columna=0
    fila=0
    i=0
    while fila < len(mediciones)-n +1:
        tabla_prom=[]
        while columna < len(mediciones[0]):
            suma=0
            while i<n:  
                try:
                   suma = (suma + mediciones[fila+i][columna])
                except:
                    tabla_prom.append('NA') 
                    i=n     #Con esto termino el loop si encuentra un NA
                i = i +1
            promedio = round(suma/n,2)
            if i !=n+1: #Con esto evito que appendee el promedio cuando la función cae en el except
                tabla_prom.append(promedio)
            columna = columna +1
            i=0
        tabla_promedio.append(tabla_prom)
        fila = fila +1
        columna=0
    return tabla_promedio

def time(tiempo):
    tiempo1=[]
    for i in tiempo:
        tiempo1.append(datetime.datetime.strptime(i, '%Y-%m-%dT%H:%M:%S'))
    return tiempo1

def dif_tiempo(tiempo,n):
    i=0
    t=0
    time=[]
    while i< len(tiempo)-n+1:
        t1= tiempo[i]
        t2= tiempo[i+n-1]
        t=((t2 - t1).total_seconds())
        time.append(t)
        i=i+ 1
    return time

def salida(tabla,t):
    A=[]
    i=0
    while i < len(t):
        A.append([t[i]] + tabla[i:][0])
        i=i+1
    return A
    
def guardardatos(tablasalida, salida):
    i=0
    salida = open(nombre_salida, "w")
    while i<len(tablasalida):
        j=0
        linea=[]
        while j <len(tablasalida[i]):
            linea.append(str(tablasalida[i][j]))
            j+=1
        linea.append("\n")
        linea= ",".join(linea)
        salida.write(linea)
        i+=1
    salida.close()
    return salida

datos= separararchivo(archivo)
tiempo= [columna[0] for columna in datos] 
mediciones= [columna[1:] for columna in datos]
mediciones=mediciones_float(mediciones)
tabla=promedio(mediciones,n)
tiempo=time(tiempo)
salidatiempo=dif_tiempo(tiempo,n)
tablasalida=salida(tabla,salidatiempo)
guardardatos(tablasalida, nombre_salida)
    